import { Document, Types } from "mongoose";

export type FullTask =
  | (Document<
      unknown,
      {},
      {
        type: string;
        status: string;
        userId: string;
        title: string;
        description: string;
        amount: number;
        dates: Types.DocumentArray<{
          start: string;
          end?: string | null | undefined;
        }>;
        taskAvailableDate?: string | null | undefined;
        relatedTaskId?: string | null | undefined;
      }
    > & {
      type: string;
      status: string;
      userId: string;
      title: string;
      description: string;
      amount: number;
      dates: Types.DocumentArray<{
        start: string;
        end?: string | null | undefined;
      }>;
      taskAvailableDate?: string | null | undefined;
      relatedTaskId?: string | null | undefined;
    } & { _id: Types.ObjectId })
  | null;
