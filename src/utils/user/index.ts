import { FullUser } from "../../types/user";

export type TransformUserForResponse = {
  nickname: string;
  email: string;
  balance: number;
  avatar?: string | null | undefined;
};

export const transformUserForResponse = (
  user: NonNullable<FullUser>,
): TransformUserForResponse => ({
  nickname: user.nickname,
  email: user.nickname,
  balance: user.balance,
  avatar: user?.avatar,
});

export const createRequestWithdrawalMessage = (
  nickname: string,
  amount: number,
) =>
  `Пользователь ${nickname} запрашивает вывод средств в количестве ${amount} рублей`;
