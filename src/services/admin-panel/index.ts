import { TaskFilter } from "../../types/admin-panel";
import TaskModel from "../../models/task";
import { ApiError, HttpCode } from "../../exceptions/ApiError";
import UserModel from "../../models/user";
import { notEmpty } from "../../utils/common";
import { FullTask } from "../../types/task";
import AuthService from "../auth";
import { transformTaskForResponse } from "../../utils/task";
import { transformUserForResponse } from "../../utils/user";
import { createAvatarLink } from "../../utils/admin-panel";

const AdminPanelService = {
  async getTasksByFilter({ filter }: { filter: TaskFilter }) {
    const perPage = 10;

    const filterArray = [
      { userId: String(filter.user.value) },
      String(filter.type.value) !== "all"
        ? { type: String(filter.type.value) }
        : undefined,
      String(filter.status.value) !== "all"
        ? { status: String(filter.status.value) }
        : undefined,
    ].filter(notEmpty);

    const tasksCount = await TaskModel.countDocuments({
      $and: filterArray,
    });

    const tasks: FullTask[] = await TaskModel.find({
      $and: filterArray,
    })
      .skip(perPage * (filter.page - 1))
      .limit(perPage);

    if (!tasks) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    if (filter?.query && filter?.query?.length) {
      const filteredByQuery = [...tasks].filter(
        (el) =>
          !!el?.title
            .toLowerCase()
            .match(new RegExp(filter.query?.toLowerCase() || ""))?.length,
      );
      return {
        items: filteredByQuery
          ?.filter(notEmpty)
          ?.map((el) => transformTaskForResponse(el)),
        pages:
          tasksCount > perPage
            ? Math.ceil(filteredByQuery.length / perPage)
            : 1,
      };
    }

    return {
      items: tasks?.filter(notEmpty)?.map((el) => transformTaskForResponse(el)),
      pages: tasksCount > perPage ? Math.ceil(tasksCount / perPage) : 1,
    };
  },
  async getUsers() {
    const users = await UserModel.find();

    if (!users) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return users;
  },
  async deleteTaskById({ taskId }: { taskId: string }) {
    await TaskModel.deleteOne({ _id: taskId });
  },
  async getTaskById({ taskId }: { taskId: string }) {
    const task = await TaskModel.findOne({ _id: taskId });

    if (!task) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return transformTaskForResponse(task);
  },
  async editTask({
    taskId,
    type,
    title,
    description,
    amount,
    status,
    relatedTaskId,
  }: {
    taskId: string;
    type: string;
    title: string;
    description: string;
    amount: number;
    status: string;
    relatedTaskId?: string;
  }) {
    await TaskModel.updateOne(
      { _id: taskId },
      { type, title, description, amount, status, relatedTaskId },
    );
  },
  async getAllUsersData({ page }: { page: number }) {
    const perPage = 10;

    const pages = await UserModel.countDocuments();

    const users = await UserModel.find()
      .skip(perPage * (page - 1))
      .limit(perPage);

    if (!users) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return {
      items: users?.filter(notEmpty)?.map(transformUserForResponse),
      pages: pages > perPage ? Math.ceil(users.length / perPage) : 1,
    };
  },
  async deleteUserById({ userId }: { userId: string }) {
    await UserModel.deleteOne({ _id: userId });
  },
  async getUserById({ userId }: { userId: string }) {
    const user = await UserModel.findOne({ _id: userId });

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return transformUserForResponse(user);
  },
  async editUser({
    userId,
    email,
    nickname,
    balance,
  }: {
    userId: string;
    email: string;
    nickname: string;
    balance: string;
  }) {
    await UserModel.updateOne({ _id: userId }, { email, nickname, balance });
  },
  async uploadUserAvatarByUserId({
    userId,
    filename,
  }: {
    userId: string;
    filename: string;
  }) {
    const user = await UserModel.findOne({ _id: userId });

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    const avatarLink = createAvatarLink(filename);

    await UserModel.updateOne({ _id: userId }, { avatar: avatarLink });
  },
  async createUser({
    nickname,
    email,
    password,
  }: {
    nickname: string;
    email: string;
    password: string;
  }) {
    await AuthService.registration({
      nickname,
      email,
      password,
    });
  },
};

export default AdminPanelService;
