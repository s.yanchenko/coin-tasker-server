import { Request, Response } from "express";
import { ApiError, ApiErrorRes, HttpCode } from "../../exceptions/ApiError";
import AdminPanelService from "../../services/admin-panel";

const AdminPanelController = {
  async getTasksByFilter(req: Request, res: Response) {
    try {
      const { filter } = req.body;

      const tasks = await AdminPanelService.getTasksByFilter({ filter });

      return res.status(HttpCode.success).send(tasks);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getUsers(req: Request, res: Response) {
    try {
      const users = await AdminPanelService.getUsers();

      const usersSelectItems = users.map((el) => ({
        label: el.nickname,
        value: el._id,
      }));

      return res.status(HttpCode.success).send(usersSelectItems);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async deleteTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      await AdminPanelService.deleteTaskById({ taskId });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const task = await AdminPanelService.getTaskById({ taskId });

      return res.status(HttpCode.success).send(task);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async editTask(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const { type, title, description, amount, status, relatedTaskId } =
        req.body;

      await AdminPanelService.editTask({
        taskId,
        type,
        title,
        description,
        amount,
        status,
        relatedTaskId,
      });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getAllUsersData(req: Request, res: Response) {
    try {
      const { page } = req.body;

      const userData = await AdminPanelService.getAllUsersData({ page });

      return res.status(HttpCode.success).send(userData);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async deleteUserById(req: Request, res: Response) {
    try {
      const { userId } = req.params;

      await AdminPanelService.deleteUserById({ userId });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getUserById(req: Request, res: Response) {
    try {
      const { userId } = req.params;

      const user = await AdminPanelService.getUserById({ userId });

      return res.status(HttpCode.success).send(user);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async editUser(req: Request, res: Response) {
    try {
      const { userId } = req.params;

      const { email, nickname, balance } = req.body;

      await AdminPanelService.editUser({
        userId,
        email,
        nickname,
        balance,
      });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async uploadUserAvatarByUserId(req: Request, res: Response) {
    try {
      const { userId } = req.params;

      if (!req?.file?.filename) {
        throw new ApiError({
          httpCode: HttpCode.notFound,
        });
      }

      await AdminPanelService.uploadUserAvatarByUserId({
        userId,
        filename: req.file.filename,
      });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async createUser(req: Request, res: Response) {
    try {
      const { nickname, email, password } = req.body;

      await AdminPanelService.createUser({ nickname, email, password });

      return res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
};

export default AdminPanelController;
