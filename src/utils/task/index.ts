import { FullTask } from "../../types/task";
import { Types } from "mongoose";

export type TransformTaskForResponse = {
  id: Types.ObjectId;
  type: string;
  title: string;
  description: string;
  amount: number;
  status: string;
  dates: { start: string; end: string | null | undefined }[] | null | undefined;
  taskAvailableDate?: string | null | undefined;
  relatedTaskId?: string | null | undefined;
};

export const transformTaskForResponse = (
  task: NonNullable<FullTask>,
): TransformTaskForResponse => ({
  id: task._id,
  type: task.type,
  title: task.title,
  description: task.description,
  amount: task?.amount,
  status: task?.status,
  dates: task?.dates?.map((el) => ({ start: el.start, end: el.end })),
  taskAvailableDate: task?.taskAvailableDate,
  relatedTaskId: task?.relatedTaskId,
});
