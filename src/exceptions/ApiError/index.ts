export enum HttpCode {
  success = 200,
  badRequest = 400,
  unauthorized = 401,
  forbidden = 403,
  notFound = 404,
  internalServerError = 500,
}

export type ApiErrorRes = {
  httpCode: HttpCode;
  description?: string;
};

export class ApiError {
  public readonly httpCode;
  public readonly description;

  constructor({
    httpCode,
    description,
  }: {
    httpCode: number;
    description?: string | string[];
  }) {
    this.httpCode = httpCode;
    this.description = description;
  }
}
