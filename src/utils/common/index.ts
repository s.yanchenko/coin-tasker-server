export function notEmpty<TValue>(
  value: TValue | null | undefined,
): value is TValue {
  return value !== null && value !== undefined;
}

export const compareDates = (
  startDate: Date,
  endDate: Date,
  operator: "<" | ">" | "=" | "<=" | ">=",
) => {
  const startDateTimestamp = new Date(startDate.toDateString()).getTime();
  const endDateTimestamp = new Date(endDate.toDateString()).getTime();

  switch (operator) {
    case "<":
      return startDateTimestamp < endDateTimestamp;
    case ">":
      return startDateTimestamp > endDateTimestamp;
    case "=":
      return startDateTimestamp === endDateTimestamp;
    case "<=":
      return startDateTimestamp <= endDateTimestamp;
    case ">=":
      return startDateTimestamp >= endDateTimestamp;
    default:
      return false;
  }
};

export const clearLinkDate = (date: Date) => new Date(date.toISOString());

export const addDay = (date: Date, count: number) =>
  new Date(clearLinkDate(date).setDate(date.getDate() + count));

export const resetTime = (date: Date) =>
  new Date(
    new Date(
      new Date(new Date(date.setSeconds(0)).setMilliseconds(0)).setMinutes(0),
    ).setHours(0),
  );
