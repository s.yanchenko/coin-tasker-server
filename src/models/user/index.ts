import { model, Schema } from "mongoose";

const UserSchema = new Schema({
  password: { type: String, required: true },
  nickname: { type: String, required: true },
  email: { type: String, unique: true, required: true },
  balance: { type: Number, required: true },
  totalBalance: { type: Number, required: false },
  roles: { type: [String], required: false },
  avatar: { type: String, required: false },
  availableTasksTodayCount: { type: Number, required: true },
  lastRequestWithdrawal: { type: String, required: false },
  isAvailableRequestWithdrawal: { type: Boolean, required: true },
  availableRequestWithdrawalAmount: { type: Number, required: true },
});

const UserModel = model("user", UserSchema);

export default UserModel;
