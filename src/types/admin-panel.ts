export type SelectItem = { label: string; value: string | number };

export type TaskFilter = {
  status: SelectItem;
  type: SelectItem;
  user: SelectItem;
  page: number;
  query?: string;
};
