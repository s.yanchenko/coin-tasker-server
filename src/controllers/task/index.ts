import { Request, Response } from "express";
import { ApiErrorRes, HttpCode } from "../../exceptions/ApiError";
import { TaskService } from "../../services/task";
import { TransformTaskForResponse } from "../../utils/task";

export const TaskController = {
  async getExecutionTasks(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies;

      const executionTasks = await TaskService.getExecutionTasks({
        accessToken,
      });

      res.status(HttpCode.success).send(executionTasks);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async changeTrackerStatus(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const { date, status } = req.body;

      await TaskService.changeTrackerStatus({
        taskId,
        date,
        status,
      });

      res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const task = await TaskService.getTaskById({
        taskId,
      });

      res.status(HttpCode.success).send(task);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async startTaskById(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const { date } = req.body;

      await TaskService.startTaskById({
        taskId,
        date,
      });

      res.status(HttpCode.success).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getAvailableTasks(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies;

      const availableTasks: TransformTaskForResponse[] =
        await TaskService.getAvailableTasks({
          accessToken,
        });

      res.status(200).send(availableTasks);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async getTaskQuestionsById(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const questions = await TaskService.getTaskQuestionsById({ taskId });

      res.status(200).send(questions);
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async checkTaskQuestions(req: Request, res: Response) {
    try {
      const { taskId } = req.params;

      const { answers } = req.body;

      const { isTakingQuiz } = await TaskService.checkTaskQuestions({
        taskId,
        answers,
      });

      res.status(200).send({ isTakingQuiz });
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async createSuggestedTask(req: Request, res: Response) {
    try {
      const { type, title, description, amount } = req.body;

      await TaskService.createSuggestedTask({
        type,
        title,
        description,
        amount,
      });

      res.status(200).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
  async finishTaskById(req: Request, res: Response) {
    try {
      const { accessToken } = req.cookies;

      const { amount } = req.body;

      await TaskService.finishTaskById({
        amount,
        accessToken,
      });

      res.status(200).send();
    } catch (error: unknown) {
      const apiError = error as ApiErrorRes;
      res
        .status(apiError?.httpCode || 500)
        .send({ description: apiError.description });
    }
  },
};
