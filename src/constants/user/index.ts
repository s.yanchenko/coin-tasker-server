export const UserRoles = {
  admin: "admin",
} as const;

export const maxAvailableRequestWithdrawalAmount = 15000;
