import express from "express";
import AdminPanelController from "../../controllers/admin-panel";
import { initiateMulterStorage } from "../../utils/mongoose";
import { AdminRoleMiddleware } from "../../middlewares/user";

const router = express.Router();

const upload = initiateMulterStorage();

router.post(
  "/get-tasks-by-filter",
  AdminRoleMiddleware,
  AdminPanelController.getTasksByFilter,
);
router.get("/get-users", AdminRoleMiddleware, AdminPanelController.getUsers);
router.delete(
  "/delete-task-by-id/:taskId",
  AdminRoleMiddleware,
  AdminPanelController.deleteTaskById,
);
router.get(
  "/get-task-by-id/:taskId",
  AdminRoleMiddleware,
  AdminPanelController.getTaskById,
);
router.post(
  "/edit-task/:taskId",
  AdminRoleMiddleware,
  AdminPanelController.editTask,
);
router.post(
  "/get-all-users-data",
  AdminRoleMiddleware,
  AdminPanelController.getAllUsersData,
);
router.delete(
  "/delete-user-by-id/:userId",
  AdminRoleMiddleware,
  AdminPanelController.deleteUserById,
);
router.get(
  "/get-user-by-id/:userId",
  AdminRoleMiddleware,
  AdminPanelController.getUserById,
);
router.post(
  "/edit-user/:userId",
  AdminRoleMiddleware,
  AdminPanelController.editUser,
);
router.post(
  "/upload-user-avatar/:userId",
  upload.single("avatar"),
  AdminRoleMiddleware,
  AdminPanelController.uploadUserAvatarByUserId,
);
router.post(
  "/create-user",
  AdminRoleMiddleware,
  AdminPanelController.createUser,
);

export default router;
