import { Document, Types } from "mongoose";

export type FullQuestion = Document<
  unknown,
  {},
  {
    taskId: string;
    questions: Types.DocumentArray<{
      title: string;
      variants: Types.DocumentArray<{ title: string }>;
      rightOption: string;
    }>;
  }
> & {
  taskId: string;
  questions: Types.DocumentArray<{
    title: string;
    variants: Types.DocumentArray<{ title: string }>;
    rightOption: string;
  }>;
} & { _id: Types.ObjectId };
