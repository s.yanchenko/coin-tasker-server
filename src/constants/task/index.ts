export enum TaskTypes {
  book = "book",
  fitness = "fitness",
  video = "video",
  telegram = "telegram",
  film = "film",
  other = "other",
}

export enum TaskStatus {
  available = "available",
  done = "done",
  paused = "paused",
  progress = "progress",
  onApproval = "on-approval",
}

export const maxAvailableTasksTodayCount = 5;
