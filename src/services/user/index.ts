import UserModel from "../../models/user";
import { ApiError, HttpCode } from "../../exceptions/ApiError";
import {
  coreErrors,
  exceededMaxWithdrawalRequestAmountErrorMessage,
  telegramBotUndefinedErrorMessage,
  withdrawalRequestDeniedErrorMessage,
} from "../../exceptions/messages";
import { TokenService } from "../token";
import { JwtPayload } from "jsonwebtoken";
import {
  createRequestWithdrawalMessage,
  transformUserForResponse,
} from "../../utils/user";
import { createAvatarLink } from "../../utils/admin-panel";
import TaskModel from "../../models/task";
import { TaskStatus } from "../../constants/task";
import { initiateBot } from "../../utils/telegram-bot";
import { UserRoles } from "../../constants/user";
import TelegramBotModel from "../../models/telegram-bot";

export const UserService = {
  async uploadAvatar({ _id, filename }: { _id: string; filename: string }) {
    const user = await UserModel.findOne({
      _id,
    });

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound || 500,
        description: coreErrors.notFound,
      });
    }

    const avatarLink = createAvatarLink(filename);

    await UserModel.updateOne({ _id }, { avatar: avatarLink });
  },
  async getBalance({ accessToken }: { accessToken: string }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const user = await UserModel.findOne({
      _id: (tokenData as JwtPayload)._id,
    });

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return {
      balance: user?.balance || 0,
      isAvailableRequestWithdrawal: user.isAvailableRequestWithdrawal,
      lastRequestWithdrawal: user?.lastRequestWithdrawal,
      availableRequestWithdrawalAmount: user?.availableRequestWithdrawalAmount,
    };
  },
  async getProfileData({ accessToken }: { accessToken: string }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const user = await UserModel.findOne({
      _id: (tokenData as JwtPayload)._id,
    });

    const totalCompletedTasksCount = await TaskModel.find({
      $and: [
        { userId: (tokenData as JwtPayload)._id },
        { status: TaskStatus.done },
      ],
    }).countDocuments();

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return {
      ...transformUserForResponse(user),
      totalCompletedTasksCount,
      totalBalance: user?.totalBalance || 0,
    };
  },
  async editProfile({
    accessToken,
    filename,
    nickname,
  }: {
    accessToken: string;
    filename?: string;
    nickname?: string;
  }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    if (nickname?.length) {
      await UserModel.updateOne(
        { _id: (tokenData as JwtPayload)._id },
        { nickname },
      );
    }

    if (filename) {
      await UserService.uploadAvatar({
        _id: (tokenData as JwtPayload)._id,
        filename,
      });
    }
  },
  async requestWithdrawal({
    accessToken,
    amount,
  }: {
    accessToken: string;
    amount: number;
  }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const userId = (tokenData as JwtPayload)._id;

    const user = await UserModel.findOne({ _id: userId });

    const admin = await UserModel.findOne({
      roles: { $in: [UserRoles.admin] },
    });

    if (!admin) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    if (!user.isAvailableRequestWithdrawal) {
      throw new ApiError({
        httpCode: HttpCode.forbidden,
        description: withdrawalRequestDeniedErrorMessage,
      });
    }

    if (
      amount > user.availableRequestWithdrawalAmount &&
      amount > user.balance
    ) {
      throw new ApiError({
        httpCode: HttpCode.forbidden,
        description: exceededMaxWithdrawalRequestAmountErrorMessage,
      });
    }

    const bot = initiateBot();

    if (!bot) {
      throw new ApiError({
        httpCode: HttpCode.internalServerError,
        description: telegramBotUndefinedErrorMessage,
      });
    }

    const adminChatData = await TelegramBotModel.findOne({
      userId: String(admin._id),
    });

    if (!adminChatData) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    await bot.sendMessage(
      adminChatData.chatId,
      createRequestWithdrawalMessage(user.nickname, amount),
    );

    const currentDay = new Date().toISOString();

    await UserModel.updateOne(
      { _id: user.id },
      {
        lastRequestWithdrawal: currentDay,
        isAvailableRequestWithdrawal: false,
        balance: user.balance - amount,
        availableRequestWithdrawalAmount:
          user.availableRequestWithdrawalAmount - amount,
      },
    );
  },
};
