import express from "express";
import { AuthMiddleware } from "../../middlewares/auth";
import { TaskController } from "../../controllers/task";

const router = express.Router();

router.get(
  "/available-tasks",
  AuthMiddleware,
  TaskController.getAvailableTasks,
);
router.get(
  "/execution-tasks",
  AuthMiddleware,
  TaskController.getExecutionTasks,
);
router.post(
  "/start-task/:taskId",
  AuthMiddleware,
  TaskController.startTaskById,
);
router.post(
  "/change-tracker-status/:taskId",
  AuthMiddleware,
  TaskController.changeTrackerStatus,
);
router.get(
  "/get-task-by-id/:taskId",
  AuthMiddleware,
  TaskController.getTaskById,
);
router.get(
  "/task-questions/:taskId",
  AuthMiddleware,
  TaskController.getTaskQuestionsById,
);
router.post(
  "/check-task-questions/:taskId",
  AuthMiddleware,
  TaskController.checkTaskQuestions,
);
router.post(
  "/finish-task/:taskId",
  AuthMiddleware,
  TaskController.finishTaskById,
);
router.post(
  "/create-suggested-task",
  AuthMiddleware,
  TaskController.createSuggestedTask,
);
export default router;
