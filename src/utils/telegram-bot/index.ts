import { ApiError, HttpCode } from "../../exceptions/ApiError";
import TelegramApi from "node-telegram-bot-api";
import dotenv from "dotenv";

dotenv.config();

export const initiateBot = () => {
  try {
    const token = process.env.TELEGRAM_API_TOKEN;

    if (!token) {
      throw new ApiError({
        httpCode: HttpCode.internalServerError,
      });
    }

    return new TelegramApi(token, { polling: true });
  } catch (e) {
    console.log(e);
  }
};
