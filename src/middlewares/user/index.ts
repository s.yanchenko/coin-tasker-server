import { NextFunction, Request, Response } from "express";
import { ApiError, ApiErrorRes, HttpCode } from "../../exceptions/ApiError";
import { TokenService } from "../../services/token";
import { JwtPayload } from "jsonwebtoken";
import { UserRoles } from "../../constants/user";

export const AdminRoleMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { accessToken } = req.cookies;

    if (!accessToken) {
      throw new ApiError({ httpCode: HttpCode.unauthorized });
    }

    const tokenData = TokenService.getVerifyAccessToken({
      accessToken,
    });

    if (!Boolean(tokenData)) {
      throw new ApiError({ httpCode: HttpCode.unauthorized });
    }

    if (
      !(tokenData as JwtPayload).roles.some(
        (el: string) => el === UserRoles.admin,
      )
    ) {
      throw new ApiError({ httpCode: HttpCode.forbidden });
    }

    next();
  } catch (error: unknown) {
    const apiError = error as ApiErrorRes;
    res
      .status(apiError?.httpCode || 500)
      .send({ description: apiError?.description });
  }
};
