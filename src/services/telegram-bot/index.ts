import { ApiError, HttpCode } from "../../exceptions/ApiError";
import { initiateBot } from "../../utils/telegram-bot";
import { telegramBotUndefinedErrorMessage } from "../../exceptions/messages";
import UserModel from "../../models/user";
import TelegramBotModel from "../../models/telegram-bot";

export const listenTelegramBotStartMessage = () => {
  try {
    const bot = initiateBot();

    const emailRegexp = /^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i;

    if (!bot) {
      throw new ApiError({
        httpCode: HttpCode.internalServerError,
        description: telegramBotUndefinedErrorMessage,
      });
    }

    bot.on("message", async (message) => {
      if (message.text === "/start") {
        await bot.sendMessage(message.chat.id, "Введи свой Email");
      }
      if (emailRegexp.test(message.text || "")) {
        const email = message.text;

        const user = await UserModel.findOne({ email });

        if (!user) {
          return bot.sendMessage(
            message.chat.id,
            "Мы не нашли пользователя с таким Email",
          );
        }

        const chat = await TelegramBotModel.findOne({
          userId: String(user._id),
        });

        if (chat) {
          return bot.sendMessage(message.chat.id, "Ты уже зарегистрирован");
        }

        await TelegramBotModel.create({
          userId: user?._id,
          chatId: message.chat.id,
        });

        await bot.sendMessage(message.chat.id, "Ты успешно зарегистрирован");
      }
    });
  } catch (e) {
    console.log(e);
  }
};
