import { model, Schema } from "mongoose";

const TelegramBotSchema = new Schema({
  userId: { type: String, required: true },
  chatId: { type: String, required: true },
});

const TelegramBotModel = model("telegram-bot", TelegramBotSchema);

export default TelegramBotModel;
