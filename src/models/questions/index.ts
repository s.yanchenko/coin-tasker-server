import { Schema, model } from "mongoose";

const QuestionItemSchema = new Schema({
  title: { type: String, required: true },
  variants: [
    {
      title: { type: String, required: true },
    },
  ],
  rightOption: { type: String, required: true },
});

const QuestionsSchema = new Schema({
  taskId: { type: String, required: true },
  questions: [QuestionItemSchema],
});

const QuestionsModel = model("questions", QuestionsSchema);

export default QuestionsModel;
