import { TokenService } from "../token";
import TaskModel from "../../models/task";
import { JwtPayload } from "jsonwebtoken";
import { ApiError, HttpCode } from "../../exceptions/ApiError";
import { maxAvailableTasksTodayCount, TaskStatus } from "../../constants/task";
import { FullTask } from "../../types/task";
import { notEmpty } from "../../utils/common";
import { transformTaskForResponse } from "../../utils/task";
import QuestionsModel from "../../models/questions";
import UserModel from "../../models/user";
import { FullQuestion } from "../../types/questions";

export const TaskService = {
  async getExecutionTasks({ accessToken }: { accessToken: string }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const currentTasks = await TaskModel.find({
      $and: [
        { userId: (tokenData as JwtPayload)._id },
        {
          $or: [{ status: TaskStatus.progress }, { status: TaskStatus.paused }],
        },
      ],
    });

    if (!currentTasks) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return currentTasks
      .map((el) => (notEmpty(el) ? transformTaskForResponse(el) : undefined))
      .filter(notEmpty);
  },
  async changeTrackerStatus({
    taskId,
    date,
    status,
  }: {
    taskId: string;
    date: string;
    status: string;
  }) {
    let currentTask: FullTask = await TaskModel.findOne({
      _id: taskId,
    });

    if (!currentTask) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    const lengthDatesArray = currentTask?.dates?.length;

    const idLastDatesItem = currentTask?.dates[lengthDatesArray - 1]?._id;

    if (status === TaskStatus.progress && idLastDatesItem) {
      await TaskModel.updateOne(
        { _id: currentTask._id, "dates._id": idLastDatesItem },
        {
          status: TaskStatus.paused,
          $set: {
            "dates.$.end": date,
          },
        },
      );
    }

    if (status === TaskStatus.paused) {
      await TaskModel.updateOne(
        { _id: currentTask._id },
        {
          status: TaskStatus.progress,
          dates: [...currentTask?.dates, { start: date, end: null }],
        },
      );
    }
  },
  async getTaskById({ taskId }: { taskId: string }) {
    const task = await TaskModel.findOne({
      _id: taskId,
    });

    if (!task) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    return transformTaskForResponse(task);
  },
  async startTaskById({ taskId, date }: { taskId: string; date: string }) {
    const currentTask = await TaskModel.findOne({
      _id: taskId,
    });

    if (!currentTask) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    const currentUser = await UserModel.findOne({
      _id: currentTask.userId,
    });

    if (!currentUser) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    await TaskModel.updateOne(
      { _id: currentTask._id },
      { status: TaskStatus.progress, dates: { start: date, end: null } },
    );

    await UserModel.updateOne(
      { _id: currentTask.userId },
      { availableTasksTodayCount: currentUser.availableTasksTodayCount - 1 },
    );
  },
  async getAvailableTasks({ accessToken }: { accessToken: string }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const userId = (tokenData as JwtPayload)._id;

    const availableTasks: FullTask[] = [];

    const user = await UserModel.findOne({ _id: userId });

    const availableTasksTodayCount =
      maxAvailableTasksTodayCount - (user?.availableTasksTodayCount || 0);

    /* Поиск доступных заданий, которые не были выполнены и не были запущены, но уже были показаны */
    const availableTasksWithAvailableDate: FullTask[] = await TaskModel.find({
      $and: [
        { userId },
        {
          $and: [
            { status: TaskStatus.available },
            { taskAvailableDate: { $exists: true } },
          ],
        },
      ],
    });

    availableTasks.push(
      ...availableTasksWithAvailableDate.slice(0, maxAvailableTasksTodayCount),
    );

    if (availableTasks.length < availableTasksTodayCount) {
      /* Поиск выполненных задач у которых есть связанная задача */
      const completedTasksWithRelatedTaskId: FullTask[] = await TaskModel.find({
        $and: [
          { userId },
          {
            $and: [
              { status: TaskStatus.done },
              { relatedTaskId: { $exists: true } },
            ],
          },
        ],
      });

      const relatedIds: string[] = completedTasksWithRelatedTaskId
        ?.map((el) => el?.relatedTaskId)
        ?.filter(notEmpty);

      if (relatedIds.length) {
        /* Поиск задач из массива связей, которые не были ещё запущены и выполнены */
        const relatedTasks: FullTask[] = await TaskModel.find({
          $and: [
            { userId },
            { _id: { $in: relatedIds } },
            {
              $and: [
                { status: TaskStatus.available },
                { taskAvailableDate: { $exists: false } },
              ],
            },
          ],
        });

        const idsAvailableTasksWithAvailableDate =
          availableTasksWithAvailableDate?.map((el) => el?._id);

        const filteredRelatedTasks = relatedTasks.filter(
          (el) => !idsAvailableTasksWithAvailableDate?.includes(el?._id),
        );

        const neededFilteredRelatedTasks = filteredRelatedTasks.slice(
          0,
          maxAvailableTasksTodayCount - (availableTasks.length - 1),
        );

        availableTasks.push(...neededFilteredRelatedTasks);

        availableTasks.splice(maxAvailableTasksTodayCount);

        await TaskModel.updateMany(
          { _id: { $in: neededFilteredRelatedTasks?.map((el) => el?._id) } },
          {
            taskAvailableDate: new Date().toISOString(),
          },
        );
      }

      if (availableTasks.length < availableTasksTodayCount) {
        /* Поиск задач, которые доступны для выполнения и ещё не были показаны пользователю */
        const newAvailableTasks: FullTask[] = await TaskModel.find({
          $and: [
            { userId },
            {
              $and: [
                { status: TaskStatus.available },
                { taskAvailableDate: { $exists: false } },
              ],
            },
          ],
        });

        const idsAvailableTasksWithAvailableDate =
          availableTasksWithAvailableDate?.map((el) => el?._id);

        const filteredNewAvailableTasks = newAvailableTasks.filter(
          (el) => !idsAvailableTasksWithAvailableDate?.includes(el?._id),
        );

        const neededFilteredNewAvailableTasks = filteredNewAvailableTasks.slice(
          0,
          maxAvailableTasksTodayCount - (availableTasks.length - 1),
        );

        availableTasks.push(...neededFilteredNewAvailableTasks);

        availableTasks.splice(maxAvailableTasksTodayCount);

        await TaskModel.updateMany(
          {
            _id: { $in: neededFilteredNewAvailableTasks?.map((el) => el?._id) },
          },
          {
            taskAvailableDate: new Date().toISOString(),
          },
        );
      }
    }

    return availableTasks
      .map((el) => (notEmpty(el) ? transformTaskForResponse(el) : undefined))
      .filter(notEmpty);
  },
  async getTaskQuestionsById({ taskId }: { taskId: string }) {
    return QuestionsModel.findOne({ taskId });
  },
  async checkTaskQuestions({
    taskId,
    answers,
  }: {
    taskId: string;
    answers: { questionId: string; answerOption: string }[];
  }) {
    const questions: FullQuestion | null = await QuestionsModel.findOne({
      taskId,
    });

    if (!questions) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    let rightAnswersCount = 0;

    questions.questions.forEach((question) => {
      const findAnswer = answers.find(
        (answer) => answer.questionId === String(question._id),
      );

      if (question.rightOption === findAnswer?.answerOption) {
        rightAnswersCount++;
      }
    });

    return { isTakingQuiz: rightAnswersCount === questions.questions.length };
  },
  async createSuggestedTask({
    type,
    title,
    description,
    amount,
  }: {
    type: string;
    title: string;
    description: string;
    amount: number;
  }) {
    await TaskModel.create({
      type,
      title,
      description,
      amount,
    });
  },
  async finishTaskById({
    amount,
    accessToken,
  }: {
    amount: number;
    accessToken: string;
  }) {
    const tokenData = TokenService.getVerifyAccessToken({ accessToken });

    const userId = (tokenData as JwtPayload)._id;

    const user = await UserModel.findOne({ _id: userId });

    if (!user) {
      throw new ApiError({
        httpCode: HttpCode.notFound,
      });
    }

    await UserModel.updateOne(
      { _id: userId },
      { totalBalance: user.balance + amount, balance: user.balance + amount },
    );
  },
};
