import express, { Express } from "express";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import methodOverride from "method-override";
import auth from "./src/routers/auth";
import { initiateMongoConnection } from "./src/utils/mongoose";
import user from "./src/routers/user";
import cors from "cors";
import task from "./src/routers/task";
import adminPanel from "./src/routers/admin-panel";
import { listenTelegramBotStartMessage } from "./src/services/telegram-bot";
import cron from "node-cron";
import UserModel from "./src/models/user";
import { maxAvailableRequestWithdrawalAmount } from "./src/constants/user";
import { maxAvailableTasksTodayCount } from "./src/constants/task";

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.use(cookieParser());
app.use(methodOverride());

app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(bodyParser.json());

app.use("/api/auth", express.json(), cors({ credentials: true }), auth);
app.use("/api/user", express.json(), cors({ credentials: true }), user);
app.use("/api/task", express.json(), cors({ credentials: true }), task);
app.use(
  "/api/admin-panel",
  express.json(),
  cors({ credentials: true }),
  adminPanel,
);

(async () => {
  await initiateMongoConnection();
  app.listen(port, () => console.log(`Server started on PORT = ${port}`));
})();

listenTelegramBotStartMessage();

cron.schedule("0 8 * * *", () => {
  UserModel.updateMany(
    {},
    { availableTasksTodayCount: maxAvailableTasksTodayCount },
  );
});

cron.schedule("* * 6 * *", () => {
  UserModel.updateMany(
    {},
    {
      isAvailableRequestWithdrawal: true,
      availableRequestWithdrawalAmount: maxAvailableRequestWithdrawalAmount,
    },
  );
});

cron.schedule("* * 21 * *", () => {
  UserModel.updateMany({}, { isAvailableRequestWithdrawal: true });
});
