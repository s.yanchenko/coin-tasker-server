import { Document, Types } from "mongoose";

export type FullUser =
  | (Document<
      unknown,
      {},
      {
        password: string;
        nickname: string;
        email: string;
        balance: number;
        totalBalance?: number | null | undefined;
        roles?: string[] | null | undefined;
        avatar?: string | null | undefined;
        availableTasksTodayCount: number;
        availableRequestWithdrawalAmount: number;
        isAvailableRequestWithdrawal: boolean;
        lastRequestWithdrawal?: string | null | undefined;
      }
    > & {
      password: string;
      nickname: string;
      email: string;
      balance: number;
      totalBalance?: number | null | undefined;
      roles?: string[] | null | undefined;
      avatar?: string | null | undefined;
      availableTasksTodayCount: number;
      availableRequestWithdrawalAmount: number;
      isAvailableRequestWithdrawal: boolean;
      lastRequestWithdrawal?: string | null | undefined;
    } & { _id: Types.ObjectId })
  | null;
