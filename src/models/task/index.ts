import { Schema, model } from "mongoose";

const TaskSchema = new Schema({
  userId: { type: String, required: true },
  type: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  amount: { type: Number, required: true },
  status: { type: String, required: true },
  dates: [
    {
      start: { type: String, required: true },
      end: { type: String, required: false },
    },
  ],
  taskAvailableDate: { type: String, required: false },
  relatedTaskId: { type: String, required: false },
});

const TaskModel = model("task", TaskSchema);

export default TaskModel;
