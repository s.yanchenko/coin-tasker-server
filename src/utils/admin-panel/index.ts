export const createAvatarLink = (filename: string) =>
  `${process.env.API_URL || "http://localhost:5000"}/user/avatar/${filename}`;
